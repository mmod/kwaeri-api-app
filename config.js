/**
 * config.js
 *
 * @package: kwaeri-api-app
 */


 // INCLUDES
 //import { path } from 'path';


 // DEFINES


let config =
{
    // Development configuration
    development:
    {
        local: true,
        domain: 'api.kwaeri.io',
        forceAdminSSL: true,  // @todo change to forcessl: { admin: boolean;, app: boolean; }
        mail:
        {
            fromaddress: 'noreply@kwaeri.co',
            transport: 'SMTP',
            options:  // @todo rename to service
            {
                service: 'Gmail',  // @todo rename to provider
                auth:  // @Todo rename to authentication
                {
                    user: 'test@gmail.com',
                    pass: 'password'
                }
            }
        },
        app:
        {
            base: __dirname,
            root: '',
            controller_path: '/app/controllers',
            model_path: '/app/models',
            view_path: '/app/views',
            asset_path: '/assets',
            asset_provider: 'mmod',
            layout_path: '/layouts',
            theme: 'mmod/mmxrm',
            extension_path: '/extensions'
        },
        database:
        {
            client: 'mysql',
            host: 'localhost',
            port: 3306,
            database: 'mmxrmdev',
            user: 'mmdadm',
            password: '^DevPass777$',
            debug: true
        },
        server:
        {
            host: '0.0.0.0',
            port:
            {
                app: '7719',
                admin: '7717'
            }
        }
    },

    // Production configuration
    production:
    {
        url: 'api.kwaeri.io',
        forceAdminSSL: true,
        mail:
        {
            fromaddress: 'noreply@kwaeri.io',
            transport: 'SMTP',
            options:
            {
                service: 'Gmail',
                auth:
                {
                    user: 'test@gmail.com',
                    pass: 'password'
                }
            }
        },
        app:
        {
            base: __dirname,
            root: '',
            controller_path: '/app/controllers',
            model_path: '/app/models',
            view_path: '/app/views',
            asset_path: '/assets',
            asset_provider: 'mmod',
            layout_path: '/layouts',
            theme: 'mmod/mmxrm',
            extension_path: '/extensions'
        },
        database:
        {
            client: 'mysql',
            host: 'localhost',
            port: 3306,
            database: 'mmxrmpro',
            user: 'mmpadm',
            password: '^ProPass777$',   // Passwords should be strong like so, but this is obviously a bad one to use...
            debug: false
        },
        server:
        {
            host: '0.0.0.0',
            port:
            {
                app: '7720',
                admin: '7718'
            }
        }
    },
};


// Use module.exports so that we have the direct context of the exported object, regardless of type.
module.exports = exports = config;
