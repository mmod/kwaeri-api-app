/*-----------------------------------------------------------------------------
 * @package:    kwaeri-api-app
 * @author:     <Your Name>
 * @copyright:  2015-2018 <Copyright Holder>
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    1.0.0
 *---------------------------------------------------------------------------*/


 // INCLUDES:
 let config = require( './config' ),
 nodekit = require( '@kwaeri/node-kit' );


// DEFINES:
let nk = new nodekit.nodekit( config );


// Start the application:
nk.init();