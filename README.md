# kwaeri-api-app

[![pipeline status](https://gitlab.com/mmod/kwaeri-api-app/badges/master/pipeline.svg)](https://gitlab.com/mmod/kwaeri-api-app/commits/master)  [![coverage report](https://gitlab.com/mmod/kwaeri-api-app/badges/master/coverage.svg)](https://mmod.gitlab.io/kwaeri-api-app/coverage/)

A project template for creating front-end (client-side) applications using React.js..

## TOC
* [Implementation Details](#implementation-details)
* [Build Instructions](#build-instructions)
  * [Git the Source](#git-the-source)
  * [Install Dependencies](#install-dependencies)
  * [Build the Application](#build-the-application)
  * [Test the application](#test-the-application)
* [Other Considerations](#other-considerations)
  * [Gitflow](#gitflow)
  * [Continuous Integration and Deployment](#continuous-integration-and-deployment)
  * [A Kwaeri API](#a-kwaeri-api)
* [Tools](#tools)

## Implementation Details

The application shell consists of the following (Features have support noted):

* NPM for project dependency management.
  * Babel
  * Gulp.
  * @kwaeri/node-kit
  * @kwaeri/platform derivatives included
* Babel for ES6 syntax support
  * @babel 7+ leveraged
  * @babel/preset-env leveraged
  * Object rest spread supported
  * Polyfill included.
* Gulp for build processes
  * gulp-babel used for bulk transpile of source
  * Source code in `src` directory compiled to `app` directory
  * del used for clean process (removes `app` directory)
  * gulp-bump-version used for automated project and source file header version management
* @kwaeri/Node-kit for application platform
  * Configuration in root directory enables @kwaeri/node-kit as an application platform
  * Serves the application
  * Provides session management for the application
  * Routes the application (dynamically)
  * Provides direct response from Controllers for supporting API architecture
  * Provides server-side rendering for supporting advanced application architecture
  * Provides database integration (MySQL and PostgreSQL included, custom drivers supported)
  * Provides a CLI for easing process invlved with application development.
* nkm for a general project cli
  * Enables intial creation and seeding of database from the command line (using a seed).
  * Enables the creation of database migrations from the command line (generates migration files.)
  * Enables the execution of database migrations from the command line (up or down)
  * Enables the creation of projects from the command line (can generate an API or React project)
  * Enables the creation of API endpoints (can generate template endpoints within an API project)
  * Enables the creation of React components (can generate presentation or container components within a React project)
  * Enables the creation of a default administrative user (when using the default user system)
* @kwaeri/platform derivatives included for rapid development
  * Provides context-based user management
  * Provides an authentication system
  * Provides acces control

  | Stage | Feature                    | Status  |
  |:------|:---------------------------|:--------|
  | SC    | Typescript Support         | NQY     |
  | SC    | ES6 Support via Babel      | WORKING |
  | SC    | Gulp builds application    | WORKING |
  | SC    | Gulp cleans application    | WORKING |
  | SC    | Gulp manages versions      | WORKING |
  | CF    | Server                     | WORKING |
  | CF    | Sessions                   | WORKING |
  | CF    | Router                     | WORKING |
  | CF    | Database                   | WIP     |
  | CF    | Direct Response (API ARCH) | WORKING |
  | CF    | Sever-Side Rendering       | WIP     |
  | CLI   | Generate Projects          | WIP     |
  | CLI   | Generate Endpoints         | WIP     |
  | CLI   | Generate Components        | WORKING |
  | CLI   | Generate Migrations        | TBA     |
  | CLI   | Generate Passwords         | TBA     |
  | CLI   | Generate Admin User        | TBA     |

**Legends**

* **Stage**
  * **SC**: Source Control. This represents a feature related to source control. (i.e. is Typescript supported in user-defined source code?).
  * **CF**: Core Functionality. This represents a core function provided to the application by @kwaeri/node-kit. (i.e. Does it work to serve an API?).
  * **CLI**: CLI Feature. This represents a planned CLI feature for @kwaeri/node-kit. (i.e. Is the Node-kit CLI functional for generating API endpoints?)

* **Status**
  * **NQY**: It's planned, but a long way off...there's more important work to do for now.
    * However, you could probably figure it out yourself easily enough - we're already transpiling the es6 javascript using babel from `src` to `app`. Browse the Gulpfile.
  * **WORKING**: It's working 100% and mostly reliable.
  * **TBA**: To be annonced when it's a WIP.

## Build Instructions

The following steps should be taken in order to build this application:

### Git the Source

Clone the repository from [Gitlab](https://gitlab.com/mmod/kwaeri-react-app):

*via HTTPS*:

```bash
git clone https://gitlab.com/mmod/kwaeri-react-app
```

*via Git+SSH*:

```bash
git clone git@gitlab.com:mmod/kwaeri-react-app
```

### Install dependencies

The following dependencies are required:

* Node.js (latest version is ideal)
  * NPM, which will come pre-bundled with Node.js.

You may use Yarn if you like, however, NPM will suffice.

Head back to your terminal, and proceed to install the additional dependencies as follows:

```bash
cd kwaeri-react-app
npm install .
```

### Build the Application

Now that you have all the dependencies installed, run the following command to build the application:

```bash
npm run build
```

That's it! You've successfully built the application.

### Test the Application

There are two different ways you can test the application:

#### Development

To test the application as a developer, you may leverage the tests which are provided with the source. The tests are built using ?.

Run the following command:

```bash
npm test
```

More to come...

#### Production

Open `...TBD...` in your favorite browser (*Chrome recommended*) to test the application.

## Other Considerations

As a compliment to the code itself, you could also make use of some common features of Gitlab for performing common project management tasks, as well as in making use of best practices.

To do so, leverage the Project Management features of Gitlab in the following ways:

### Gitflow

* The [project board](https://gitlab.com/mmod/kwaeri-api-app/boards?=) can be leveraged for creating and managing [user-sceanrios/tasks](https://gitlab.com/mmod/kwaeri-api-app/issues), and [milestones](https://gitlab.com/mmod/kwaeri-api-app/milestones) to guide the completion of your project.
  * [Issue-First Development](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) is a perfect candidate for driving the management of your application.

### Continuous Integration and Deployment

Leverage the job/pipeline features of Gitlab in the following ways:

* When checking in the master branch, a [pipline](https://gitlab.com/mmod/kwaeri-api-app/pipelines) may be utilized in testing, building, and deploying your application.
  * [Jobs](https://gitlab.com/mmod/kwaeri-react-app/-/jobs) may be dispatched with every commit to master; Test and Deploy. [Here](https://gitlab.com/mmod/kwaeri-react-app/blob/master/.gitlab-ci.yml)'s an example configuration.

### A Kwaeri API

You can review the [Kwaeri API Application](https://mmod.gitlab.io/kwaeri-api-app/), continuously deployed to Gitlab pages upon a check-in to master.

## Tools

The tools leveraged in building this template include:

* [Visual Studio Code](https://code.visualstudio.com/)