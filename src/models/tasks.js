/*-----------------------------------------------------------------------------
 * @package:    kwaeri-api-app
 * @author:     <Your Name>
 * @copyright:  2015-2018 <Copyright Holder>
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    1.0.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import { kdt } from '@kwaeri/developer-tools';
//import { Driver } from '@kwaeri/driver/build/src/driver';


// DEFINES
let _ = new kdt();


/**
 * Defines the context model
 *
 * @since 1.0.0
 */
export default class TasksModel
{

    constructor()
    {
        this.schema = {
            id: [ true, 'int', 'User Id'],
            type: [ true, 'int', 'Account Type' ],
            acl: [ true, 'int', 'Access Level' ],
            ts: [ true, 'text', 'Member Since' ],
            username: [ true, 'text', 'Username' ],
            password: [ true, 'text', 'Password' ],
            context: [ true, 'int', 'Default Context' ]
        };
    }

    async fetchAll( request, response, callback, klay = null )
    {
        // Prep the database object:
        let db = this.dbo();

        // The query for admin users is slightly different. We also join the
        // elevated table and fetch the stored password from there. This is
        // because if the user was created as an admin then the users password
        // will be associated with their id within the elevated table. If they
        // are properly given access to the app as an admin, the elevated table
        // will be populated respectively. It's simply another security check
        // for the admin site.
        let result =  await db
        .query( "select * from tasks" );

        console.log( 'db result: ' );
        console.log( result.rows );

        // And invoke the callback, passing the result of our query
        if( _.type( callback ) === 'function' )
        {
            // Asynchronous
            callback( request, response, JSON.stringify( result.rows ) );
        }
        else
        {
            // Synchronous
            return result.rows;
        }
    }
}

// Use module.exports so that we have the direct context of the exported object, regardless of type.
//module.exports = exports = TasksModel;
