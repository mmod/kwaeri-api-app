/*-----------------------------------------------------------------------------
 * @package:    kwaeri-api-app
 * @author:     <Your Name>
 * @copyright:  2015-2018 <Copyright Holder>
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    1.0.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import '@babel/polyfill';
import TasksModel from '../models/tasks';


// DEFINES



/**
 * Constructor
 *
 * @since 1.0.0
 */
export default class TasksController
{
    constructor() {}


    // HTTP GET /tasks
    index( request, response )
    {

        let ci = this;
        let tasksModel =  new TasksModel();

        let model = this.model.set( tasksModel );

        // Here we define the callback for our all() method
        let callback = function( req, res, buffer )
        {
            ci.respond( req, res, buffer );
        };

        model.fetchAll( request, response, callback );
        // This should be self-explanatory:
        //if( !request.isAuthenticated )
        //{
        //    response.redirect( '/account/login' );
        //    return;
        //}

        // When we are acting as an API, we do not prepare an HTML response
        // of any kind. Because of this, we do not have a use for klay - as
        // klay is used in turning out a content-rendered response
        // this.klay //...(not used)

        // We do not 'render' anything:
        // this.render( request, response );

        // We CAN make use of models, and prepare data in any fashion we
        // may require...Here we'll simplify things:

        // To provide a most basic implementation of an 'API', we'll follow
        // the Google JSON Specification, and leave out any validation
        // techniques:
        //
        /*
        let buffer = {
            "apiVersion": "1.0",
            "paramsPassed": JSON.stringify( request.requrl.query ),
            "data": {
                "updated": "2018-10-02T01:10:54.001Z",
                "totalItems": 2,
                "startIndex": 1,
                "itemsPerPage": 10,
                "items": [
                    {
                        "id": "1",
                        "created": "",
                        "updated": "",
                        "title": "Create an API for the application",
                        "description": "As a developer, I'd expect that there will exist an API for the front-end application to pull its data from...",
                        "tags": [
                            "backend",
                            "database",
                            "feature"
                        ],
                        "weight": 1.0,
                        "rating": 4.75,
                        "ratingCount": 97,
                        "viewCount": 100,
                        "favoriteCount": 10,
                        "commentCount": 15,
                        "commentsAllowed": true
                    },
                    {
                        "id": "2",
                        "created": "",
                        "updated": "",
                        "title": "Build an endpoint for tasks",
                        "description": "As a developer I'd expect that a React-based taskboard would have an API that provides an endpoint for dealing with tasks...",
                        "tags": [
                            "backend",
                            "database",
                            "feature",
                            "endpoint",
                            "tasks"
                        ],
                        "weight": 1.0,
                        "rating": 4.45,
                        "ratingCount": 107,
                        "viewCount": 201,
                        "favoriteCount": 19,
                        "commentCount": 27,
                        "commentsAllowed": true
                    }
                ]
            }
        };

        // For API handling, we 'respond' directly - with out asking the
        // renderer to do anything for us. This could be called in a callback
        // that is passed to a model if we need to do any data fetching or
        // preparations:
        this.respond( request, response, buffer );
        //
        */
    }


    //HTTP GET /tasks/add
    add( request, response )
    {
    }


    //HTTP POST /tasks/add
    addPost( request, response )
    {
    }


    //HTTP GET /tasks/edit
    edit( request, response )
    {
    }


    //HTTP POST /tasks/edit
    editPost( request, response )
    {
    }


    //HTTP POST /tasks/delete
    deletePost( request, response )
    {
    }
}


// Export
//module.exports = exports = TasksController;
